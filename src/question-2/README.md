<div align="center">
  <img src="src/assets/images/store-space-logo.png" alt="Store Space" width="500" />
</div>
<br>
<div align="center"><h3>Store Space challenge</h3></div>
<br>
<div align="center">
  [Links](#links)&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  [Technologies](#tech)&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  [Configuration](#config)&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  [Tests](#tests)&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  [Other commands](#other)&nbsp;&nbsp;&nbsp;
</div>

## :link: <a name="links">Links</a>

- ##### [Hosted Project](https://daniel-store-space-challenge.netlify.app/)

## :rocket: <a name="tech">Technologies</a>

- [React](https://reactjs.org)
- [Typescript](https://www.typescriptlang.org/)
- [Create-react-app](https://create-react-app.dev/)
- [Jest](https://jestjs.io/)
- [React-testing-Library](https://testing-library.com/)
- [Styled-components](https://styled-components.com/)
- [Axios](https://github.com/axios/axios)

## :wrench: <a name="config">Configuration</a>

#### :exclamation: node version: v16.14.2 and npm version: 8.5.0 

#### 1. Install all dependencies with

```sh
$ npm install 
```

#### 2. Start the development server

```sh
$ npm run start
```
## :mag: <a name="tests">Tests</a>

#### 1. Run jest and testing-library

```
$ npm run test
```

## :gift: <a name="other">Other commands</a>

```sh
# Build for web (compiled to build)
$ npm run build

# Eject the project
$ npm run eject
```
