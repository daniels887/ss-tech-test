const response = {
  "current_page": 1,
  "data": [
      {
          "fact": "Ancient Egyptian family members shaved their eyebrows in mourning when the family cat died.",
          "length": 91
      },
      {
          "fact": "During the Middle Ages, cats were associated with withcraft, and on St. John’s Day, people all over Europe would stuff them into sacks and toss the cats into bonfires. On holy days, people celebrated by tossing cats from church towers.",
          "length": 235
      },
      {
          "fact": "Tests done by the Behavioral Department of the Musuem of Natural History conclude that while a dog's memory lasts about 5 minutes, a cat's recall can last as long as 16 hours.",
          "length": 175
      },
      {
          "fact": "The term “puss” is the root of the principal word for “cat” in the Romanian term pisica and the root of secondary words in Lithuanian (puz) and Low German puus. Some scholars suggest that “puss” could be imitative of the hissing sound used to get a cat’s attention. As a slang word for the female pudenda, it could be associated with the connotation of a cat being soft, warm, and fuzzy.",
          "length": 387
      },
      {
          "fact": "A cat called Dusty has the known record for the most kittens. She had more than 420 kittens in her lifetime.",
          "length": 108
      },
      {
          "fact": "A cat has more bones than a human being; humans have 206 and the cat has 230 bones.",
          "length": 83
      },
      {
          "fact": "Florence Nightingale owned more than 60 cats in her lifetime.",
          "length": 61
      },
      {
          "fact": "A cat rubs against people not only to be affectionate but also to mark out its territory with scent glands around its face. The tail area and paws also carry the cat’s scent.",
          "length": 174
      },
      {
          "fact": "Cats have \"nine lives\" thanks to a flexible spine and powerful leg and back muscles",
          "length": 83
      }
  ],
  "first_page_url": "https://catfact.ninja/facts?page=1",
  "from": 1,
  "last_page": 37,
  "last_page_url": "https://catfact.ninja/facts?page=37",
  "links": [
      {
          "url": null,
          "label": "Previous",
          "active": false
      },
      {
          "url": "https://catfact.ninja/facts?page=1",
          "label": "1",
          "active": true
      },
      {
          "url": "https://catfact.ninja/facts?page=2",
          "label": "2",
          "active": false
      },
      {
          "url": "https://catfact.ninja/facts?page=3",
          "label": "3",
          "active": false
      },
      {
          "url": "https://catfact.ninja/facts?page=4",
          "label": "4",
          "active": false
      },
      {
          "url": "https://catfact.ninja/facts?page=5",
          "label": "5",
          "active": false
      },
      {
          "url": "https://catfact.ninja/facts?page=6",
          "label": "6",
          "active": false
      },
      {
          "url": "https://catfact.ninja/facts?page=7",
          "label": "7",
          "active": false
      },
      {
          "url": "https://catfact.ninja/facts?page=8",
          "label": "8",
          "active": false
      },
      {
          "url": "https://catfact.ninja/facts?page=9",
          "label": "9",
          "active": false
      },
      {
          "url": "https://catfact.ninja/facts?page=10",
          "label": "10",
          "active": false
      },
      {
          "url": null,
          "label": "...",
          "active": false
      },
      {
          "url": "https://catfact.ninja/facts?page=36",
          "label": "36",
          "active": false
      },
      {
          "url": "https://catfact.ninja/facts?page=37",
          "label": "37",
          "active": false
      },
      {
          "url": "https://catfact.ninja/facts?page=2",
          "label": "Next",
          "active": false
      }
  ],
  "next_page_url": "https://catfact.ninja/facts?page=2",
  "path": "https://catfact.ninja/facts",
  "per_page": "9",
  "prev_page_url": null,
  "to": 9,
  "total": 332
}

export default response;
