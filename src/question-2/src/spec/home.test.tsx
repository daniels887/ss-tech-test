import { render, screen } from '@testing-library/react';
import { Home } from '../pages';
import MockAdapter from 'axios-mock-adapter';
import api from '../services/api';
import { apiResponse } from './fixture';

describe('testing home', () => {
  let mockApi: MockAdapter;

  beforeEach(() => {
    mockApi = new MockAdapter(api);
  });
  afterEach(() => {
    mockApi.restore();
  });
  
  it('should render 9 facts', async () => {
    mockApi
    .onGet(
      `/facts?limit=9`
    )
    .reply(200, apiResponse);
    
    render(<Home />);

    const factsList = await screen.findByTestId('list');

    expect(factsList.childElementCount).toBe(9);
  })
})
