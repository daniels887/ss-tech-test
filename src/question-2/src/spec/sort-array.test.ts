import { sortArray } from "../utils";

describe('Test sort array', () => {
  it('should sorted alphabetically', () => {
    const disorderedArray = [
      { fact: 'Dog', length: 3 }, 
      { fact: 'Horse', length: 5 }, 
      { fact: 'Cat', length: 3 }
    ];
    
    const expectedArray = [
      { fact: 'Cat', length: 3 },
      { fact: 'Dog', length: 3 }, 
      { fact: 'Horse', length: 5 } 
    ];
    expect(sortArray(disorderedArray)).toEqual(expectedArray);
  })
})
