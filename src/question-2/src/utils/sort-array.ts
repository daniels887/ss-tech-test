import { Fact } from "../typings/fact"

/**
 * 
 * @param array An array of object with Fact[] type.
 * @returns An array sorted alphabetically
 */
const sortArray = (array: Fact[]): Fact[] => {
  return array.sort((a: Fact ,b: Fact) => {
    return a.fact.localeCompare(b.fact)
  })
};

export default sortArray;
