import { FaSpinner } from 'react-icons/fa';
import styled, { keyframes } from 'styled-components';

const rotate = keyframes`
  from {
    transform: rotate(0deg);
  }
  to {
    transform: rotate(360deg);
  }
`;

export const Container = styled.div`
  max-width: 1020px;
  margin: 0 auto;
  padding: 0 20px 20px 20px;
`;

export const Title = styled.h1`
  text-align: center;
  font-size: 42px;
`;

export const List = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 313px);
  grid-gap: 20px;
  justify-content: center;
  align-items: center;
  height: 800px;
  @media (max-width: 599px) {
    grid-template-columns: repeat(1, 100%);
  }
`;

export const Card = styled.div`
  background: #DDD;
  border-radius: 8px;
  height: 250px;
  overflow-y: auto;
  color: black;
  box-shadow: 0px 0px 10px 0px #ddd;
`;

export const Image = styled.img`
  height: 120px;
  width: 100%;
  object-fit: cover;
`
export const Description = styled.div`
  padding: 8px;
`;

export const Loading = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;

  svg {
    animation: ${rotate} 2s linear infinite;
  }
`;


export const Spinner = styled(FaSpinner)``;
