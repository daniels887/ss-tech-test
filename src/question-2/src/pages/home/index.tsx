import { useEffect, useState } from "react";
import { Card, Container, List, Title, Image, Description, Loading, Spinner } from "./styles";
import api from "../../services/api";
import { Fact } from "../../typings/fact";
import { sortArray } from "../../utils";

const Home = () => {
  const [listFacts, setListFacts] = useState<Fact[]>([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    (async () => {
      setLoading(true);
      const response = await api.get('/facts?limit=9');
      setListFacts(sortArray(response.data.data));
      setLoading(false);
    })()
  }, [])

  if(loading) {
    return (
      <Loading>
        <Spinner size={48} />
      </Loading>
    )
  }

  return (
    <Container>
      <Title>Cat facts</Title>
      <List data-testid="list">
        {listFacts.length > 0 && listFacts.map(({ fact }, index) => (
          <Card key={index}>
            <Image src={require(`../../assets/images/${index + 1}.jpg`)}/>
            <Description>
              {fact}
            </Description>
          </Card>
        ))}
      </List>
    </Container>
  )
}

export default Home;
