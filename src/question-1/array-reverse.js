const reverseArray = (array) => {
  const reversedArray = [];
  for(let i = array.length - 1; i >= 0; i--) {
    reversedArray.push(array[i])
  }

  return reversedArray;
}

const fruits = ['Apple', 'Banana', 'Orange', 'Coconut']

console.log('source -', fruits)
console.log('return -', reverseArray(fruits))
